set nocompatible
set bs=2
set t_Co=256
set history=5000
filetype off



" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'Valloric/YouCompleteMe'

Plugin 'ctrlpvim/ctrlp.vim'
Bundle 'tpope/vim-fugitive.git'
Plugin 'jlanzarotta/bufexplorer'

Bundle 'altercation/vim-colors-solarized.git'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

Plugin 'octol/vim-cpp-enhanced-highlight'
Bundle 'rhysd/vim-clang-format'
Plugin 'nvie/vim-flake8'

Plugin 'NLKNguyen/papercolor-theme'
Plugin 'thiagoalessio/rainbow_levels.vim'

Bundle 'jamessan/vim-gnupg'

Bundle 'rdnetto/YCM-Generator'
call vundle#end()

filetype plugin indent on

set textwidth=80
set colorcolumn=+1

set t_Co=256
set background=dark
let g:solarized_termcolors=256
let g:solarized_termtrans=1
" colorscheme solarized
colorscheme PaperColor

nmap <Leader>C :ClangFormatAutoToggle<CR>
let g:clang_format#detect_style_file = 1

syntax on
"Highlight whitespace errors
highlight link WhiteSpaceError ErrorMsg
"Flag trailing whitespace as an error
au Syntax * syn match WhiteSpaceError /\(\zs\%#\|\s\)\+$/ display
"Flag mixed leading space/tabs as an error
au Syntax * syn match WhiteSpaceError /^ \+\ze\t/ display

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" Use Unix as the standard file type
set ffs=unix,dos,mac

" Turn backup off, since most stuff is in SVN, git et.c anyway...
set nobackup
set nowb
set noswapfile

set cursorline
set showcmd
set ruler
set number
set relativenumber

" Ignore case when searching
set ignorecase
" When searching try to be smart about cases 
set smartcase
" Makes search act like search in modern browsers
set incsearch
" Highlight search results
set hlsearch
" Disable highlight when <leader><cr> is pressed
map <silent> <leader><cr> :noh<cr>

set showmatch
"Allow maching angle brackets
set matchpairs+=<:>
set modeline
" How many tenths of a second to blink when matching brackets
set mat=2

" Don't redraw while executing macros (good performance config)
set lazyredraw 

" For regular expressions turn magic on
set magic

" Configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

set wildmode=longest,list,full
set wildmenu
set wildignore=*.o,*~,*.pyc,*.d
set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/.DS_Store

"Always show current position
set ruler

" No annoying sound on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500

" => Text, tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Do not Use spaces instead of tabs
set noexpandtab

" Be smart when using tabs ;)
set smarttab

" 1 tab == 2 spaces
set shiftwidth=2
set tabstop=2

" Linebreak on 500 characters
set lbr
set tw=500

set ai "Auto indent
set si "Smart indent
set wrap "Wrap lines

nnoremap <silent> <F5> :let _s=@/ <Bar> :%s/\s\+$//e <Bar> :let @/=_s <Bar> :nohl <Bar> :unlet _s <CR>

" => Visual mode related
""""""""""""""""""""""""""""""
" Visual mode pressing * or # searches for the current selection
" Super useful! From an idea by Michael Naumann
vnoremap <silent> * :<C-u>call VisualSelection('', '')<CR>/<C-R>=@/<CR><CR>
vnoremap <silent> # :<C-u>call VisualSelection('', '')<CR>?<C-R>=@/<CR><CR>


" With a map leader it's possible to do extra key combinations
" like <leader>w saves the current file
let mapleader = ","
let g:mapleader = ","
" Close the current buffer
map <leader>bd :Bclose<cr>:tabclose<cr>gT
" Close all the buffers
map <leader>ba :bufdo bd<cr>

map <leader>l :bnext<cr>
map <leader>h :bprevious<cr>

" Useful mappings for managing tabs
map <leader>tn :tabnew<cr>
map <leader>to :tabonly<cr>
map <leader>tc :tabclose<cr>
map <leader>tm :tabmove 
map <leader>t<leader> :tabnext 

" Return to last edit position when opening files (You want this!)
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" Set to auto read when a file is changed from the outside
set autoread

" Fast saving
nmap <leader>w :w!<cr>

" Big Save
command W w !sudo tee % > /dev/null

" Set 7 lines to the cursor - when moving vertically using j/k
set so=7

"Python Config
let python_highlight_all=1
au FileType python syn keyword pythonDecorator True None False self

au BufNewFile,BufRead *.jinja set syntax=htmljinja
au BufNewFile,BufRead *.mako set ft=mako

au FileType python map <buffer> F :set foldmethod=indent<cr>


"YCM config
let g:ycm_confirm_extra_conf=0
let g:ycm_autoclose_preview_window_after_completion=1
let g:ycm_autoclose_preview_window_after_insertion=1
"Enable location list so we can rapidly jump through errors
let g:ycm_always_populate_location_list=1

nnoremap <C-x> :YcmCompleter GoTo<CR>
nnoremap <F6> :YcmCompleter GetType<CR>

"Vim airline configuration
"Enable powerline fancy glyphs
let g:airline_powerline_fonts=1
"Always show the statusbar
set laststatus=2
"Don't show mode below statusline since airline already provides it
set noshowmode

""""""""""""""""""""""""""""""
" => bufExplorer plugin
""""""""""""""""""""""""""""""
let g:bufExplorerDefaultHelp=0
let g:bufExplorerShowRelativePath=1
let g:bufExplorerFindActive=1
let g:bufExplorerSortBy='name'
map <leader>o :BufExplorer<cr>

""""""""""""""""""""""""""""""
" => CTRL-P
""""""""""""""""""""""""""""""
let g:ctrlp_working_path_mode = 0

let g:ctrlp_map = '<c-f>'
map <leader>j :CtrlP<cr>
map <c-b> :CtrlPBuffer<cr>

let g:ctrlp_max_height = 20
let g:ctrlp_custom_ignore = 'node_modules\|^\.DS_Store\|^\.git\|^\.coffee|^\.d|^\.o'

" => Syntastic (syntax checker)
let g:syntastic_python_checkers=['pyflakes']

map <leader>; :RainbowLevelsToggle<cr>
"au FileType javascript,cpp,python,php,xml,yaml :RainbowLevelsOn

" Don't close window, when deleting a buffer
command! Bclose call <SID>BufcloseCloseIt()
function! <SID>BufcloseCloseIt()
   let l:currentBufNum = bufnr("%")
   let l:alternateBufNum = bufnr("#")

   if buflisted(l:alternateBufNum)
     buffer #
   else
     bnext
   endif

   if bufnr("%") == l:currentBufNum
     new
   endif

   if buflisted(l:currentBufNum)
     execute("bdelete! ".l:currentBufNum)
   endif
endfunction
