export IGNOREEOF=42
export HISTFILESIZE=
export HISTSIZE=
export HISTCONTROL=ignoredups:ignorespace

export GTK_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
export QT_IM_MODULE=ibus

export EDITOR=vim
export PAGER=less
export TERM=xterm-256color

shopt -s checkwinsize

eval `dircolors ~/.env/shell/.dircolors`

export PROMPT_DIRTRIM=3

GPG_TTY=`tty`
export GPG_TTY

#Colors
RED="$(tput setaf 1)"
GREEN="$(tput setaf 2)"
YELLOW="$(tput setaf 3)"
PURPLE="$(tput setaf 5)"
BLUE="$(tput setaf 6)"
ORANGE="$(tput setaf 9)"
RESET="$(tput sgr0)"

. ~/.env/git/git-prompt.sh
. ~/.env/shell/.bashrc-xrdp
. ~/.env/shell/.bashrc-redshift

#Show result of last command
export PS1='`if [ $? = 0 ]; then echo "\[${GREEN}\]+"; else echo "\[${ORANGE}\]-\[${GREEN}\]"; fi`'
#Username@host:(git branch)cwd
export PS1+='\u@\h:\[${ORANGE}\]$(__git_ps1 "[%s]")\[${YELLOW}\]\w'
#Show number of jobs if at least one job
export PS1+='`if [ -n "$(jobs)" ]; then echo "\[${BLUE}\](\j)"; fi`'
#Finish
export PS1+='\[${RESET}\]\$ '

export HISTFILESIZE=
export HISTSIZE=
export HISTTIMEFORMAT="[%F %T] "
# Change the file location because certain bash sessions truncate .bash_history file upon close.
# http://superuser.com/questions/575479/bash-history-truncated-to-500-lines-on-each-login
export HISTFILE=~/.bash_eternal_history
